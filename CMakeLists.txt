cmake_minimum_required(VERSION 2.8.3)
project(piksi_node)

find_package(catkin REQUIRED COMPONENTS message_generation roscpp std_msgs geometry_msgs sensor_msgs)

add_message_files(
  FILES
  msg_baseline_ecef.msg
  msg_baseline_ned.msg
  msg_dops.msg
  msg_gps_time.msg
  msg_pos_ecef.msg
  msg_pos_llh.msg
  msg_vel_ecef.msg
  msg_vel_ned.msg
  PiksiRTKPos.msg
  PiksiBaseline.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
  sensor_msgs
)

catkin_package(
  CATKIN_DEPENDS message_runtime roscpp std_msgs geometry_msgs sensor_msgs
)

include_directories(include ${catkin_INCLUDE_DIRS})

install(DIRECTORY launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
