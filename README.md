# Piksi Node #

ROS driver for the Piksi RTK GPS module written in Python.
Output in ROS NavSatFix format, as well as custom message types which contain the baseline and navigaion 
messages from the [SBP protocol](https://raw.githubusercontent.com/swift-nav/libsbp/v0.33/docs/sbp.pdf).

This repository has been further developed on Github by ETHZ-ASL:
https://github.com/ethz-asl/ethz_piksi_ros

### Dependencies ###

* ROS
* Piksi Tools (https://github.com/swift-nav/piksi_tools) - developed for v0.24.2

### Settings ###

The file */cfg/piksi_driver_settings.yaml* contains the driver settings. Before running the driver, the correct serial port needs to be specified (usually '/dev/ttyUSB0' or '/dev/ttyUSB1'). Furthermore, the message types to be published can be set, as well as the diagonal elements of the covariance matrix in the NavSatFix messages.

### Message Types ###

* The driver supports standard *NavSatFix* messages. For the Piksi to output global GPS coordinates in the navsatfix_rtk and piksirtkpos messages, the base position needs to be specified. This has to be done on the Piksi itself through the Piksi console settings (http://docs.swiftnav.com/wiki/Using_the_Console).
* *PiksiBaseline* messages contain a 3-Vector with the distance (in mm) between base and rover in NED-cordinates, as well as information of whether the Piksi is in RTK Float or RTK Fixed mode.
* *PiksiRtkPos* messages contain the RTK NavSatFix message, as well as information of whether the Piksi is in RTK Float or RTK Fixed mode.
* All other message types contain raw data, and have the same message format as specified in the [SBP protocol](https://raw.githubusercontent.com/swift-nav/libsbp/v0.33/docs/sbp.pdf)