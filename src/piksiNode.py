#!/usr/bin/env python

#
#  Title:        piksi_node.py
#  Description:  ROS Driver for the Piksi RTK GPS module
#

import rospy
import sys

# Import message types
from std_msgs.msg import Header
from sensor_msgs.msg import NavSatFix, NavSatStatus
from piksi_node.msg import *

# Import Piksi SBP library
from sbp.client.drivers.pyserial_driver import PySerialDriver
from sbp.client.handler import Handler
from sbp.navigation import *

import time
 
def make_callback(sbp_type, ros_message, pub, attrs):
    """
    Dynamic generator for callback functions for message types from
    the SBP library. 
    Inputs: 'sbp_type' name of SBP message type
            'ros_message' ROS message type with SBP format
            'pub' ROS publisher for ros_message
            'attrs' array of attributes in SBP/ROS message
    Returns: callback function 'callback'
    """
    def callback(msg):
        sbp_message = sbp_type(msg)
        for attr in attrs:
            setattr(ros_message, attr, getattr(sbp_message, attr))
        pub.publish(ros_message)
    return callback

def init_callback_and_publisher(topic_name, ros_datatype, sbp_msg_type, callback_data_type, *attrs):
    """
    Initializes the callback function and ROS publisher for an SBP
    message type.
    Inputs: 'topic_name' name of ROS topic for publisher
            'ros_datatype' ROS custom message type
            'sbp_msg_type' name of SBP message type for callback function
            'callback_data_type' name of SBP message type for SBP library
            '*attrs' array of attributes in ROS/SBP message
    """
    if not rospy.has_param('~publish_' + topic_name):
        rospy.set_param('~publish_' + topic_name, False)
    if rospy.get_param('~publish_' + topic_name):
        pub = rospy.Publisher(rospy.get_name() + '/' + topic_name, ros_datatype, queue_size = 10)
        ros_message = ros_datatype()
        
        # Add callback function
        callback_function = make_callback(callback_data_type, ros_message, pub, attrs)
        handler.add_callback(callback_function, msg_type=sbp_msg_type)
 
def navsatfix_callback(msg_raw):
    """
    Callback function for SBP_MSG_POS_LLH message types. Publishes
    NavSatFix messages.
    """
    msg = MsgPosLLH(msg_raw)

    navsatfix_msg.header.stamp = rospy.Time.now()
    navsatfix_msg.latitude = msg.lat
    navsatfix_msg.longitude = msg.lon
    navsatfix_msg.altitude = msg.height
    
    # SPP GPS messages
    if msg.flags == 0 and publish_spp:
        navsatfix_msg.status.status = NavSatStatus.STATUS_FIX
        navsatfix_msg.position_covariance = [var_spp_x, 0, 0,
                                             0, var_spp_y, 0,
                                             0, 0, var_spp_z]
        pub_spp.publish(navsatfix_msg)
        
    # RTK GPS messages
    elif (msg.flags == 1 or msg.flags == 2) and (publish_rtk or publish_piksirtkpos):
        
        navsatfix_msg.status.status = NavSatStatus.STATUS_GBAS_FIX
        navsatfix_msg.position_covariance = [var_rtk_x, 0, 0,
                                             0, var_rtk_y, 0,
                                             0, 0, var_rtk_z]
        if publish_rtk:
            pub_rtk.publish(navsatfix_msg)
        if publish_piksirtkpos:
            if msg.flags == 1:
                piksirtkpos_msg.mode_fixed = True
            elif msg.flags == 2:
                piksirtkpos_msg.mode_fixed = False
            piksirtkpos_msg.navsatfix = navsatfix_msg
            pub_piksirtkpos.publish(piksirtkpos_msg)
            
def baseline_callback(msg_raw):
    """
    Callback function for SBP_MSG_BASELINE_NED message types.
    Publishes PiksiBaseline messages.
    """
    msg = MsgBaselineNED(msg_raw)

    baseline_msg.header.stamp = rospy.Time.now()
    baseline_msg.baseline.x = msg.n
    baseline_msg.baseline.y = msg.e
    baseline_msg.baseline.z = msg.d
    baseline_msg.mode_fixed = msg.flags

    pub_piksibaseline.publish(baseline_msg)


# Main function.    
if __name__ == '__main__':   
    rospy.init_node('piksi')
    
    # Open a connection to Piksi using the default baud rate (1Mbaud)
    serial_port = rospy.get_param('~serial_port')

    try:
        driver = PySerialDriver(serial_port, baud=1000000)
    except SystemExit:
        rospy.logerr("Piksi not found on serial port '%s'", serial_port)
    
    # Create a handler to connect Piksi driver to callbacks
    handler = Handler(driver.read, driver.write, verbose=True)
    
    # Read settings
    var_spp_x = rospy.get_param('~var_spp_x')
    var_spp_y = rospy.get_param('~var_spp_y')
    var_spp_z = rospy.get_param('~var_spp_z')
    var_rtk_x = rospy.get_param('~var_rtk_x')
    var_rtk_y = rospy.get_param('~var_rtk_y')
    var_rtk_z = rospy.get_param('~var_rtk_z')

    if not rospy.has_param('~publish_navsatfix_rtk'):
        rospy.set_param('~publish_navsatfix_rtk', False)
    if not rospy.has_param('~publish_navsatfix_spp'):
        rospy.set_param('~publish_navsatfix_spp', False)
    if not rospy.has_param('~publish_piksirtkpos'):
        rospy.set_param('~publish_piksirtkpos', False)
    publish_spp = rospy.get_param('~publish_navsatfix_spp')
    publish_rtk = rospy.get_param('~publish_navsatfix_rtk')
    publish_piksirtkpos = rospy.get_param('~publish_piksirtkpos')
    publish_piksibaseline = rospy.get_param('~publish_piksibaseline')

    # Generate publisher and callback function for PiksiRtkPos messages
    if publish_spp or publish_rtk or publish_piksirtkpos:
        if publish_rtk:
            pub_rtk = rospy.Publisher(rospy.get_name() + '/navsatfix_rtk', NavSatFix, queue_size = 10)
        if publish_spp:
            pub_spp = rospy.Publisher(rospy.get_name() + '/navsatfix_spp', NavSatFix, queue_size = 10)
        if publish_piksirtkpos:
            pub_piksirtkpos = rospy.Publisher(rospy.get_name() + '/piksirtkpos', PiksiRTKPos, queue_size = 10)
            piksirtkpos_msg = PiksiRTKPos()
            
        # Define fixed attributes of the NavSatFixed message
        navsatfix_msg = NavSatFix()
        navsatfix_msg.header.frame_id = rospy.get_param('~frame_id')
        navsatfix_msg.position_covariance_type = NavSatFix.COVARIANCE_TYPE_APPROXIMATED
        navsatfix_msg.status.service = NavSatStatus.SERVICE_GPS
        
        handler.add_callback(navsatfix_callback, msg_type=SBP_MSG_POS_LLH)
    
    # Generate publisher and callback function for PiksiBaseline messages
    if publish_piksibaseline:
        pub_piksibaseline = rospy.Publisher(rospy.get_name() + '/piksibaseline', PiksiBaseline, queue_size = 10)
        baseline_msg = PiksiBaseline()
        handler.add_callback(baseline_callback, msg_type=SBP_MSG_BASELINE_NED)
    
    # Initialize Navigation messages
    init_callback_and_publisher('baseline_ecef', msg_baseline_ecef, SBP_MSG_BASELINE_ECEF, MsgBaselineECEF,  
                                'tow', 'x', 'y', 'z', 'accuracy', 'n_sats', 'flags')
    init_callback_and_publisher('baseline_ned', msg_baseline_ned, SBP_MSG_BASELINE_NED, MsgBaselineNED,  
                                'tow', 'n', 'e', 'd', 'h_accuracy', 'v_accuracy', 'n_sats', 'flags')
    init_callback_and_publisher('dops', msg_dops, SBP_MSG_DOPS, MsgDops, 'tow', 'gdop', 'pdop', 'tdop', 'hdop', 'vdop')
    init_callback_and_publisher('gps_time', msg_gps_time, SBP_MSG_GPS_TIME, MsgGPSTime, 'wn', 'tow', 'ns', 'flags')
    init_callback_and_publisher('pos_ecef', msg_pos_ecef, SBP_MSG_POS_ECEF, MsgPosECEF, 
                                'tow', 'x', 'y', 'z', 'accuracy', 'n_sats', 'flags')
    init_callback_and_publisher('pos_llh', msg_pos_llh, SBP_MSG_POS_LLH, MsgPosLLH, 
                                'tow', 'lat', 'lon', 'height', 'h_accuracy', 'v_accuracy', 'n_sats', 'flags')
    init_callback_and_publisher('vel_ecef', msg_vel_ecef, SBP_MSG_VEL_ECEF, MsgVelECEF, 
                                'tow', 'x', 'y', 'z', 'accuracy', 'n_sats', 'flags')
    init_callback_and_publisher('vel_ned', msg_vel_ned, SBP_MSG_VEL_NED, MsgVelNED, 
                                'tow', 'n', 'e', 'd', 'h_accuracy', 'v_accuracy', 'n_sats', 'flags')
    
    handler.start()
    
    # Keep script alive
    while not rospy.is_shutdown():
        rospy.sleep(0.5)